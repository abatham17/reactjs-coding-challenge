import React, { Component } from 'react';
import { MDBBtn, MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody, MDBTable, MDBTableBody, MDBTableHead
} from "mdbreact";
import axios from 'axios';
import $ from "jquery";

class MlbPage extends Component {
  
  constructor(props) {
        super(props);
        this.state = {
            nflList: []          
        };
    }

  componentDidMount() {
    var postData = {
      email: "test@test.com",
      password: "password"
    };

    let axiosConfig = {
     async:true,
     dataType: "json",
    };

    axios.get('https://app.ticketmaster.com/discovery/v2/events.json?size=10&apikey=5fXAiyK6eWahj6Xoz4YhRTnhpqWBeKzp', postData, axiosConfig)
    .then((res) => {
      console.log("RESPONSE RECEIVED: ", res.data._embedded.events);
      let events = res.data._embedded.events;
      let eventsArr = [];
      for(let i in events){        
        eventsArr.push({  name: <MDBCol><b>{events[i].name}</b><br></br><br></br><img src={events[i].images[0].url} width="300" height="200" /></MDBCol>, 
          type: events[i].type, 
          venue:events[i]._embedded.venues[0].name, 
          date:events[i].dates.start.localDate, 
          tickets: (
          <MDBBtn color="deep-orange" size="sm" href={events[i].url}
          target="_blank">
            Buy Now
          </MDBBtn>
      )});
  }

  let data_collspan = {
    columns: [
      {
        label: "Name",
        field: "name",
        sort: "asc"
      },
      {
        label: "Type",
        field: "type",
        sort: "asc"
      },
      {
        label: "Venue",
        field: "venue",
        sort: "asc"
      },
      {
        label: "Date",
        field: "date",
        sort: "asc"
      },
      {
        label: "Tickets",
        field: "tickets",
        sort: "asc"
      }
    ],
    rows: eventsArr
  };

  this.setState({nflList:data_collspan});

    })
    .catch((err) => {
      console.log("AXIOS ERROR: ", err);
    })



  }

render() {

  console.log(this.state.nflList)
  return(
         <MDBContainer className="mt-3">
     
      <MDBRow className="py-3">
        <MDBCol md="12">
          <MDBCard>
            <MDBCardBody>
              <h3>MLB Schedule - 2019</h3>
              <MDBTable hover>
                <MDBTableHead
                  columns={this.state.nflList.columns}
                  color="primary-color"
                  textWhite
                />
                <MDBTableBody rows={this.state.nflList.rows} />
              </MDBTable>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
    )
}


}

export default MlbPage;
