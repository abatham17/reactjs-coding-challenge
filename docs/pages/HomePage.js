import React from "react";
import { MDBEdgeHeader, MDBFreeBird, MDBContainer, MDBCol, MDBRow, MDBCardBody, MDBIcon, MDBNavLink } from "mdbreact";
import "./HomePage.css";
 
class HomePage extends React.Component {
  render() {
    return (
      <div>
        <MDBEdgeHeader color="indigo darken-3" />
        <MDBFreeBird>
          <MDBRow>
            <MDBCol
              md="10"
              className="mx-auto float-none white z-depth-1 py-2 px-2"
            >
              <MDBCardBody>
                <h2 className="h2-responsive mb-4">
                  <strong>Sports events of NFL and MLB</strong>
                </h2>
                <p className="pb-4">
                 Design an application that gives real-time information on current and upcoming events/games of NFL and MLB sports with buy ticket link/button. User should be able to filter w.r.t sport and date-range (from/to).The app should have option to buy the ticket of specific game/event, where user will be redirected to third party buy ticket link, on click.
                </p>
                <MDBRow className="d-flex flex-row justify-content-center row">
                 

                   <MDBNavLink
                      tag="button"
                      className="btn btn-sm indigo darken-3 text-white" 
                      to="nfl"
                    >
                      NFL
                    </MDBNavLink>

                      <MDBNavLink
                      tag="button"
                      className="btn btn-sm indigo darken-3 text-white" 
                      to="mlb"
                    >
                      MLB
                    </MDBNavLink>

                </MDBRow>
              </MDBCardBody>
            </MDBCol>
          </MDBRow>
        </MDBFreeBird>
        
      </div>
    );
  }
}

export default HomePage;
